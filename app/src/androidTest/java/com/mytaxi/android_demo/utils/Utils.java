package com.mytaxi.android_demo.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.SystemClock;

import androidx.test.espresso.ViewInteraction;

import com.mytaxi.android_demo.activities.MainActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

public class Utils {

    public static void goToHome(Activity activity) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        activity.startActivity(intent);
    }

    public static void toForeground(Activity activity) {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        activity.startActivity(intent);
    }

    public static void waitFor(ViewInteraction view ){
        long timeout = 0L;
        while (timeout < 10000) {
            try {
                ViewInteraction view1 = view;

                break;
            } catch (Exception e) {

                SystemClock.sleep(1000L);
                timeout += 1000L;
            }
        }
    }
}
