package com.mytaxi.android_demo;

import android.os.SystemClock;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.mytaxi.android_demo.activities.MainActivity;
import com.mytaxi.android_demo.views.LandingView;
import com.mytaxi.android_demo.views.LoginView;
import com.mytaxi.android_demo.views.UserView;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;

@RunWith(AndroidJUnit4.class)
public class LoginTests {

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    private String userName = "crazydog335";
    private String passWord = "venture";
    private LandingView landingView;
    private LoginView loginView;

    @Test
    public void verifyLoginWitCorrectCredentials() {
        loginView = new LoginView();
        landingView = loginView.performLogin(userName, passWord);

        SystemClock.sleep(3000);

        landingView.getLandingPageSearchBar().check(matches(isDisplayed()));

        UserView userView = landingView.goToUserProfile();
        userView.checkUserDetails(userName);
        userView.performLogout();
    }

    @Test
    public void verifyLoginWitIncorrectCredentials() {
        loginView = new LoginView();
        String invalidUserName = "fake_UserName";
        String invalidPassword = "fake_Password";
        landingView = loginView.performLogin(invalidUserName, invalidPassword);

        SystemClock.sleep(2000);

        loginView.isLoginFailed().check(matches(isDisplayed()));
        landingView.getLandingPageSearchBar().check(doesNotExist());
    }


    @Test
    public void verifyLogout() {
        loginView = new LoginView();
        landingView = loginView.performLogin(userName, passWord);

        SystemClock.sleep(3000);

        landingView.getLandingPageSearchBar().check(matches(isDisplayed()));

        UserView userView = landingView.goToUserProfile();
        userView.performLogout();
    }
}
