package com.mytaxi.android_demo;

import android.os.SystemClock;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.mytaxi.android_demo.views.DriverView;
import com.mytaxi.android_demo.views.LandingView;
import com.mytaxi.android_demo.views.LoginView;
import com.mytaxi.android_demo.activities.MainActivity;
import com.mytaxi.android_demo.views.UserView;
import com.mytaxi.android_demo.utils.Utils;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class DriverTests {

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class, true, true);

    private String userName = "crazydog335";
    private String passWord = "venture";
    private LandingView landingView;
    private LoginView loginView;
    private String searchText = "sa";
    private String driverName = "Sarah Scott";

    @Before
    public void setUp() {
        loginView = new LoginView();
        landingView = loginView.performLogin(userName, passWord);
    }

    @After
    public void tearDown() {
        UserView userView = landingView.goToUserProfile();
        userView.performLogout();
    }

    @Test
    public void verifyFindAndCallDriver() {

        SystemClock.sleep(2000);

        landingView.doSearch(searchText);

        DriverView driverView = landingView.selectDriver(activityRule, driverName);
        driverView.verifyDriverName(driverName);
        driverView.callDriver();

        Utils.goToHome(activityRule.getActivity());
        Utils.toForeground(activityRule.getActivity());
    }
}
