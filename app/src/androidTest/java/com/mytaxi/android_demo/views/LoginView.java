package com.mytaxi.android_demo.views;

import androidx.test.espresso.ViewInteraction;

import com.mytaxi.android_demo.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class LoginView {

    private ViewInteraction userName = onView(withId(R.id.edt_username));
    private ViewInteraction password = onView(withId(R.id.edt_password));
    private ViewInteraction loginButton = onView(withId(R.id.btn_login));
    private ViewInteraction failureMessage = onView(withId(R.string.message_login_fail));

    /**
     * Method to perform login
     * @param username
     * @param passWord
     * @return
     */
    public LandingView performLogin(String username, String passWord) {
        userName.perform(clearText(), typeText(username), closeSoftKeyboard());
        password.perform(clearText(), typeText(passWord), closeSoftKeyboard());
        loginButton.perform(click());

        return new LandingView();
    }

    /**
     * Method to check if login failed
     * @return
     */
    public ViewInteraction isLoginFailed() {
        return onView(withId(R.id.snackbar_text));
    }

    /**
     * Method to get landing page search bar
     * @return
     */
    public ViewInteraction getLandingPageSearchBar() {
        return onView(withId(R.id.snackbar_text));
    }
}
