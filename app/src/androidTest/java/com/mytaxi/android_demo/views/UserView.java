package com.mytaxi.android_demo.views;

import androidx.test.espresso.ViewInteraction;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class UserView {

    private ViewInteraction logoutButton = onView(withText("Logout"));

    /**
     * Method to perfomr performLogout
     */
    public void performLogout() {
        logoutButton.perform(click());
    }

    /**
     * Method to check user details
     * @param userNameToBeChecked
     */
    public void checkUserDetails(String userNameToBeChecked) {
        onView(withText(userNameToBeChecked)).check(matches(isDisplayed()));
    }
}
