package com.mytaxi.android_demo.views;


import android.os.SystemClock;

import androidx.test.espresso.ViewInteraction;

import com.mytaxi.android_demo.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

public class DriverView {

    private ViewInteraction callButton = onView(withId(R.id.fab));

    /**
     * Method to call driver
     */
    public void callDriver() {
        long timeout = 0L;
        while (timeout < 10000) {
            try {
                callButton.check(matches(isDisplayed()));
                callButton.perform(click());
                break;
            } catch (Exception e) {

                SystemClock.sleep(1000L);
                timeout += 1000L;
            }
        }

    }

    /**
     * Method to verify driver name
     * @param driverName
     */
    public void verifyDriverName(String driverName) {
        onView(allOf(withId(R.id.textViewDriverName), withText(driverName)));
    }
}
