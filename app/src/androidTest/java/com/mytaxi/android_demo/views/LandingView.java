package com.mytaxi.android_demo.views;

import android.os.SystemClock;

import androidx.test.espresso.ViewInteraction;
import androidx.test.rule.ActivityTestRule;

import com.mytaxi.android_demo.R;
import com.mytaxi.android_demo.activities.MainActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeTextIntoFocusedView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

public class LandingView {

    private ViewInteraction searchContainer = onView(withId(R.id.searchContainer));
    private ViewInteraction textSearch = onView(withId(R.id.textSearch));
    private ViewInteraction userProfileBurgerMenu = onView(withContentDescription(R.string.navigation_drawer_open));

    /**
     * Method to perform search
     * @param searchText
     */
    public void doSearch(String searchText) {
        searchContainer.perform(click());
        textSearch.perform(clearText(), typeTextIntoFocusedView(searchText));
    }

    /**
     * Method to select a driver
     * @param activityRule
     * @param DriverName
     * @return
     */
    public DriverView selectDriver(ActivityTestRule<MainActivity> activityRule, String DriverName) {

        long timeout = 0L;
        while (timeout < 10000) {
            try {
                onView(allOf(withText(DriverName)))
                        .inRoot(withDecorView(not(activityRule.getActivity().getWindow().getDecorView())))
                        .check(matches(isDisplayed())).perform(click());
                break;
            } catch (Exception e) {

                SystemClock.sleep(1000L);
                timeout += 1000L;
            }
        }
        return new DriverView();
    }

    /**
     * Method to go to user profile
     * @return
     */
    public UserView goToUserProfile() {
        userProfileBurgerMenu.perform(click());
        return new UserView();
    }

    /**
     * Method to get landing page search bar
     * @return
     */
    public ViewInteraction getLandingPageSearchBar() {
        return onView(withId(R.id.searchContainer));
    }
}